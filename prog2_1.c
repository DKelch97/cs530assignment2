#include "prog2_1.h"

TSAFELIST* tSafeConstruct() {
    TSAFELIST *myTSList = (TSAFELIST*) malloc(sizeof(TSAFELIST));
    pthread_mutex_init(&(myTSList -> mutex), NULL);
    myTSList -> head = NULL;
    myTSList -> size = 0;
    return myTSList;
}

void tSafeDestruct(TSAFELIST *listToDlt) {
    free(listToDlt);
}

void tSafeEnqueue(TSAFELIST *listToAdd, mpz_t currInt) {
    pthread_mutex_lock(&(listToAdd -> mutex));
    if ((listToAdd -> head) == NULL) {
        TSAFENODE *initHead = (TSAFENODE*) malloc(sizeof(TSAFENODE));
        mpz_init(initHead -> number);
        listToAdd -> head = initHead; 
        mpz_set(listToAdd -> head -> number, currInt);
        listToAdd -> head -> next = NULL; 
        listToAdd -> size = 1;  
        gmp_printf("%Zd\n", listToAdd -> head -> number);
        pthread_mutex_unlock(&(listToAdd -> mutex)); 
        return;
    }
    TSAFENODE *curr  = listToAdd -> head;
    while (curr -> next != NULL) {
        curr = curr -> next;    
    }
    curr -> next = (TSAFENODE*) malloc(sizeof(TSAFENODE));
    mpz_init(curr -> next -> number);
    mpz_set(curr -> next -> number, currInt);
    gmp_printf("%Zd\n", curr -> next -> number);
    curr -> next -> next = NULL;
    listToAdd -> size += 1;
    pthread_mutex_unlock(&(listToAdd -> mutex)); 
}

TSAFEDATA* tSafeDequeue(TSAFELIST *listToRmv) {
    pthread_mutex_lock(&(listToRmv -> mutex)); 
    TSAFEDATA *tempTSData;
    if (listToRmv -> size == 0) {
        tempTSData = (TSAFEDATA*) malloc(sizeof(TSAFEDATA));
        tempTSData -> isValid = 0;
        mpz_init(tempTSData -> value);
    }
    if (listToRmv -> size == 1) {
        tempTSData = (TSAFEDATA*) malloc(sizeof(TSAFEDATA));
        tempTSData -> isValid = 1;
        mpz_init(tempTSData -> value);
        mpz_set(tempTSData -> value, listToRmv -> head -> number);
        listToRmv -> head = NULL;
        listToRmv -> size = 0;
    }  
    if (listToRmv -> size > 1) {
        tempTSData = (TSAFEDATA*) malloc(sizeof(TSAFEDATA));
        tempTSData -> isValid = 1;
        mpz_init(tempTSData -> value);
        mpz_set(tempTSData -> value, listToRmv -> head -> number);
        listToRmv -> head = listToRmv -> head -> next;
        listToRmv -> size -= 1;
    } 
    pthread_mutex_unlock(&(listToRmv -> mutex)); 
    return tempTSData;  
}

void tSafePrintQueue(TSAFELIST *listToPrt) {
    printf("Now printing the contents of the queue. \n");
    TSAFENODE *curr1  = listToPrt-> head;
    while (curr1 != NULL) { 
        gmp_printf("%Zd\n", curr1 -> number);
        curr1 = curr1 -> next;     
    }
    printf("Done printing the contents of the queue. \n");
}
/*
int main(int argc, char *argv[]) {
    TSAFELIST* myTSList1 = tSafeConstruct();
    mpz_t integ;
    mpz_init_set_str (integ, "1", 10);
    tSafeEnqueue(myTSList1, integ);
    mpz_init_set_str (integ, "2", 10);
    tSafeEnqueue(myTSList1, integ);
    mpz_init_set_str (integ, "3", 10);
    tSafeEnqueue(myTSList1, integ);
    mpz_init_set_str (integ, "4", 10);
    tSafeEnqueue(myTSList1, integ);
    mpz_init_set_str (integ, "8", 10);
    tSafeEnqueue(myTSList1, integ);
    mpz_init_set_str (integ, "7", 10);
    tSafeEnqueue(myTSList1, integ);
    mpz_init_set_str (integ, "6", 10);
    tSafeEnqueue(myTSList1, integ);
    mpz_init_set_str (integ, "5", 10);
    tSafeEnqueue(myTSList1, integ);
    tSafePrintQueue(myTSList1);
    tSafeDequeue(myTSList1);
    tSafePrintQueue(myTSList1);
    tSafeDequeue(myTSList1);
    tSafeDequeue(myTSList1);
    tSafeDequeue(myTSList1);
    tSafePrintQueue(myTSList1);
    tSafeDequeue(myTSList1);
    tSafeDequeue(myTSList1);
    tSafeDequeue(myTSList1);
    tSafeDequeue(myTSList1);
    tSafePrintQueue(myTSList1);
    return 0;
}
*/
