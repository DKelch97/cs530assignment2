#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <obstack.h>
#include <pthread.h>
#include <gmp.h>
#include <assert.h>
#include "prog2_1.c"

mpz_t globalCounter;
int maxSize;
pthread_mutex_t counterguard = PTHREAD_MUTEX_INITIALIZER;
TSAFELIST *numberList;

void getNextNum(mpz_t num) {
    pthread_mutex_lock(&counterguard);  
    mpz_set(num, globalCounter);
    int increment = 1;
    mpz_add_ui(globalCounter, globalCounter, increment);
    pthread_mutex_unlock(&counterguard); 
    return;
}
void * generatePrimes(void *arg) {
    while (1) {
        mpz_t num1;
        mpz_init(num1);
        getNextNum(num1);
        if (mpz_probab_prime_p(num1, 100000) > 0) {
            pthread_mutex_lock(&counterguard);
            if (maxSize >= 1) {
                maxSize -= 1;
                tSafeEnqueue(numberList, num1);
            }
            else {
                break;
            }
            pthread_mutex_unlock(&counterguard);
        }
        else {
            continue;        
        }
    }  
    pthread_mutex_unlock(&counterguard);
    pthread_exit(NULL);
}

int main(int argc, char *argv[]) {
    printf("Assignment #2-2, Daniel Kelchner, danjoe.kelch@gmail.com\n");
    int kNumbers = atoi(argv[1]);
    long long bBits = atoi(argv[2]);
    maxSize = kNumbers;
    mpz_init(globalCounter);
    mpz_ui_pow_ui(globalCounter, 2, bBits);    
    numberList = tSafeConstruct();
    pthread_t id1;
    pthread_create(&id1, NULL, generatePrimes, NULL);
    pthread_t id2;
    pthread_create(&id2, NULL, generatePrimes, NULL);
    pthread_t id3;
    pthread_create(&id3, NULL, generatePrimes, NULL);    
    pthread_t id4;
    pthread_create(&id4, NULL, generatePrimes, NULL);
    pthread_join(id1, NULL);
    pthread_join(id2, NULL);
    pthread_join(id3, NULL);
    pthread_join(id4, NULL);
    //Rather than calling upon the dequeue method to print the items from our queue,
    //we will simply use the helper method seen below. This method is implemented 
    //in, "prog2_1.c."
    tSafePrintQueue(numberList);
}


