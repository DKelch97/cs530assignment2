Daniel Kelchner

Danjoe.kelch@gmail.com

Files Included: 


    prog2_1.h: 
               Header file containing method signatures. Included signatures are implemented in prog2_1.c.
               A copy of said file, "prog2_4.h," is also included, and was created for the purpose of 
               testing.
    prog2_1.c: 
               This program, alongside file, "prog2_3.c," serve as a basis for a queue implementation.
               While seemingly un-needed, in that it is a mere copy of prog2_1.c, prog2_3.c was created 
               with the intention of allowing me to test variations of certain implementations of functions,
               without potentially compromising correct segments of code in the original file. This same
               approach is seen in prog2_2.c.
    prog2_2.c: 
               This program generates a sequence of pseudoprime numbers based upon user information (number of 
               desired pseudoprime values, number of bits contained by each generated value). Files prog2_5.c
               and prog2_6.c are, simply, copies of prog2_3.c.

Notes:
    
    1. 
        When compiling, please do so in the following fashion, "gcc prog2_2.c -lgmp -pthread." The reason behind
        this is that, unfortunately, after a certain point, I was no longer able to modify either of my header
        files. With that being said, I couldn't modify my header file such that I could incorporate a helper 
        method which I instantiated in the corresponding c file (i.e. prog2_1.c) (void tSafePrintQueue(TSAFELIST *listToPrt).
        Notice the line #include "prog2_1.c," which can be found in prog2_2.c.
    2.
        When running the compiled version of prog2_2.c, pseudoprime numbers will be printed to the screen as they are
        being found. After all of the requested items are found, the contents of the queue are printed so as to 
        demonstrate that they have been properly enqueued.
    
