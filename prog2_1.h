#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <obstack.h>
#include <pthread.h>
#include <gmp.h>
#include <assert.h>

typedef struct tsafenode {
    struct tsafenode *next;
    mpz_t number;
} TSAFENODE;

typedef struct tsafelist {
    pthread_mutex_t mutex;
    TSAFENODE *head;
    int size;
} TSAFELIST;

typedef struct tsafereturndata {
    //True(1)/False(0) if returned data is valid.
    int isValid;
    mpz_t value;
} TSAFEDATA;

TSAFELIST* tSafeConstruct();
void tSafeDestruct(TSAFELIST *listToDlt);
void tSafeEnqueue(TSAFELIST *listToAdd, mpz_t currInt);
TSAFEDATA* tSafeDequeue(TSAFELIST *listToRmv);

